package com.example.android.googlebooksapi;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;


public class BookAdapter extends ArrayAdapter<Book> {

    public BookAdapter(Context context, ArrayList<Book> books) {
        super(context, 0, books);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_item, null);
            holder = new ViewHolder();
            holder.numberView = (TextView) convertView.findViewById(R.id.item_number);

            holder.titleView = (TextView) convertView.findViewById(R.id.title_info);
            holder.authorView = (TextView) convertView.findViewById(R.id.author_info);
            holder.publishedView = (TextView) convertView.findViewById(R.id.published_year_info);
            holder.descriptionView = (TextView) convertView.findViewById(R.id.description_info);
            holder.bookCover = (ImageView)  convertView.findViewById(R.id.cover);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Book currentPlace = getItem(position);

        holder.numberView.setText(String.valueOf(position+1));
        holder.titleView.setText(currentPlace.getTitle());

        String author = formattedAuthor(currentPlace.getAuthor().trim());
        String authorString = getContext().getString(R.string.authors);

        if (!currentPlace.getAuthor().equals("")){
            holder.authorView.setText(String.format(authorString, author));
        } else {
            holder.authorView.setText(R.string.no_author);
        }

        String dateString = getContext().getString(R.string.publishedDate);

        if (!currentPlace.getPublishDate().equals("")) {
            holder.publishedView.setText(String.format(dateString, currentPlace.getPublishDate()));
        } else {
            holder.publishedView.setText(R.string.no_published_date);
        }

        if (!currentPlace.getDescription().equals("")) {
            holder.descriptionView.setText(currentPlace.getDescription());
        } else {
            holder.descriptionView.setText(R.string.no_description);
        }

        if (!currentPlace.getThumbnail().equals("")) {
            holder.bookCover.setImageBitmap(currentPlace.getThumbnailImage());
        } else {
            holder.bookCover.setImageResource(R.drawable.noimageicon);
        }

        return convertView;
    }

    private String formattedAuthor(String author) {
        String authorToView = author.replace("[", "");
        authorToView = authorToView.replace("]", "");
        authorToView = authorToView.replace("\"", "");
        return authorToView;
    }

    static class ViewHolder {
        private TextView numberView;
        private TextView titleView;
        private TextView authorView;
        private TextView publishedView;
        private TextView descriptionView;
        private ImageView bookCover;
    }
}