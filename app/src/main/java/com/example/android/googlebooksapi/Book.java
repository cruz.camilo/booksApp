package com.example.android.googlebooksapi;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

public class Book implements Parcelable {

    private String title;
    private String author;
    private String description;
    private String publishDate;
    private String thumbnail;
    private Bitmap thumbnailImage;

    public Book(String title, String author, String description, String publishDate,
                String thumbnail, Bitmap thumbnailImage) {
        this.title = title;
        this.author = author;
        this.description = description;
        this.publishDate = publishDate;
        this.thumbnail = thumbnail;
        this.thumbnailImage = thumbnailImage;
    }

    protected Book(Parcel in) {
        title = in.readString();
        author = in.readString();
        description = in.readString();
        publishDate = in.readString();
        thumbnail = in.readString();
        thumbnailImage = in.readParcelable(Bitmap.class.getClassLoader());
    }

    public static final Creator<Book> CREATOR = new Creator<Book>() {
        @Override
        public Book createFromParcel(Parcel in) {
            return new Book(in);
        }

        @Override
        public Book[] newArray(int size) {
            return new Book[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getDescription() {
        return description;
    }

    public String getPublishDate() {
        return publishDate;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public Bitmap getThumbnailImage() {
        return thumbnailImage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(author);
        parcel.writeString(description);
        parcel.writeString(publishDate);
        parcel.writeString(thumbnail);
        parcel.writeParcelable(thumbnailImage, i);
    }
}
