package com.example.android.googlebooksapi;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final String LOG_TAG = MainActivity.class.getName();
    private static final String BASE_QUERY_URL = "https://www.googleapis.com/books/v1/volumes?q=";

    /**
     * Adapter for the list of earthquakes
     */
    private BookAdapter mAdapter;
    private TextView mEmptyStateView;
    public String QUERY_URL;
    public View circle;
    public ListView books_list;
    private List<Book> savedListBooks;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList("BOOK_LIST", (ArrayList<? extends Parcelable>) savedListBooks);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.books_list);

        books_list = (ListView) findViewById(R.id.list);
        mEmptyStateView = (TextView) findViewById(R.id.empty);
        books_list.setEmptyView(mEmptyStateView);
        circle = findViewById(R.id.loading_spinner);
        circle.setVisibility(View.GONE);

        if (savedInstanceState != null) {
            savedListBooks = savedInstanceState.getParcelableArrayList("BOOK_LIST");
            mAdapter = new BookAdapter(MainActivity.this, new ArrayList<Book>());
            books_list.setAdapter(mAdapter);
            mAdapter.addAll(savedListBooks);
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        }

        Button search = (Button) findViewById(R.id.search);

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!isOnline()) {
                    circle.setVisibility(View.GONE);
                    mEmptyStateView.setText(R.string.no_internet);
                }

                mAdapter = new BookAdapter(MainActivity.this, new ArrayList<Book>());
                books_list.setAdapter(mAdapter);
                books_list.getEmptyView().setVisibility(ListView.GONE);

                EditText query = (EditText) findViewById(R.id.query);
                String entrySearch = query.getText().toString();
                QUERY_URL = formattedEntry(entrySearch);
                BookAsyncTask getBooks = new BookAsyncTask();
                getBooks.execute(QUERY_URL);
                circle.setVisibility(View.VISIBLE);

            }
        });
    }

    public boolean isOnline() {
        // Get a reference to the ConnectivityManager to check state of network connectivity
        ConnectivityManager cm = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        // Get details on the currently active default data network
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }

    private String formattedEntry(String entry) {
        String entryToURL = entry;

        if (entryToURL.contains(" ")) {
            entryToURL = entryToURL.replace(" ", "+");
        }
        entryToURL = BASE_QUERY_URL + entryToURL;
        return entryToURL;
    }


    private class BookAsyncTask extends AsyncTask<String, Void, List<Book>> {

        @Override
        protected List<Book> doInBackground(String... urls) {
            if (urls.length < 1 || urls[0] == null) {
                return null;
            }

            List<Book> result = QueryUtils.fetchBookData(urls[0]);
            return result;
        }

        protected void onPostExecute(List<Book> data) {

            View circle = findViewById(R.id.loading_spinner);
            circle.setVisibility(View.GONE);
            mEmptyStateView.setText(R.string.no_books);
            mAdapter.clear();
            savedListBooks = data;
            if (data != null && !data.isEmpty()) {
                mAdapter.addAll(savedListBooks);
            }
        }
    }

}
